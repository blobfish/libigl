libigl is SLOW to compile, so I tried to use the static library on 2 separate occations.
Both occasions were causing 'std::bad_alloc'.
static lib is fubar at this time. 10-15-17
static lib is still fubar at this time. 03-13-19

It is not possible to use the 'header only' mode with the libigl installion because:
headers include the cpp files for header only mode, but installation does not install the cpp files.

At this point I just made libigl a subproject of cadseer.
use "-DLIBIGL_USE_STATIC_LIBRARY=OFF" in cadseer to do 'header only'

libigl's use of cmake is confusing:
In the root cmake file it sets the options to 'ON' and the includes cmake/libigl.cmake.
cmake/libigl.cmake then sets the options to 'OFF'

cmake \
-DCMAKE_BUILD_TYPE=RelWithDebInfo \
-DLIBIGL_WITH_XML=OFF \
-DLIBIGL_WITH_TRIANGLE=OFF \
-DLIBIGL_WITH_TETGEN=OFF \
-DLIBIGL_WITH_PNG=OFF \
-DLIBIGL_WITH_OPENGL=OFF \
-DLIBIGL_WITH_OPENGL_GLFW=OFF \
-DLIBIGL_WITH_OPENGL_GLFW_IMGUI=OFF \
-DLIBIGL_WITH_EMBREE=OFF \
-DLIBIGL_WITH_COMISO=OFF \
-DLIBIGL_BUILD_TUTORIALS=OFF \
-DLIBIGL_BUILD_PYTHON=OFF \
-DLIBIGL_BUILD_TESTS=OFF \
..


-DLIBIGL_USE_STATIC_LIBRARY=OFF \
